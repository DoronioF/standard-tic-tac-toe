def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, winner):
    print(f"GAME OVER {winner} has won!")
    exit()

def is_row_winner(board, row):
    winning_row = 0
    if board[0] == board[1] and board[1] == board[2]:
        winning_row = 1
        return True
    elif board[3] == board[4] and board[4] == board[5]:
        winning_row = 2
        return True
    elif board[6] == board[7] and board[7] == board[8]:
        winning_row = 3
        return True

def is_column_winner(board, column):
    winning_column = 0
    if board[0] == board[3] and board[3] == board[6]:
        winning_column = 1
        return True
    elif board[1] == board[4] and board[4] == board[7]:
        winning_column = 2
        return True
    elif board[2] == board[5] and board[5] == board[8]:
        winning_column = 3
        return True

def is_diag_winner(board, diag):
    winning_diag = 0
    if board[0] == board[4] and board[4] == board[8]:
        winning_diag = 1
        return True
    elif board[2] == board[4] and board[4] == board[6]:
        winning_diag = 2
        return True

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player
    # line 64 and 65 are meant to check if the current spaces are occupied but I was encountering some issues
    # if board[space_number] == "X" or board[space_number] == "O":
    #     print("That space is occupied!\n Please enter another space.")
        
    if is_row_winner(board, 1):
        game_over(board, current_player)
    elif is_row_winner(board, 2):
        game_over(board, current_player)
    elif is_row_winner(board, 3):
        game_over(board, current_player)
    elif is_column_winner(board, 1):
        game_over(board, current_player)
    elif is_column_winner(board, 2):
        game_over(board, current_player)
    elif is_column_winner(board, 3):
        game_over(board, current_player)
    elif is_diag_winner(board, 1):
        game_over(board, current_player)
    elif is_diag_winner(board, 2):
        game_over(board, current_player)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")

